const { response } = require('express');
const { user } = require('../models/user');
const userService = require("../services/userService");

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const user = req.body;
    const newUser = userService.validUserAdd(user);
    if (typeof newUser === "string") {
        req.err = newUser;
    }else {
        req.new = newUser;
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    const data = req.body;
    if (!data) {
        req.err = "Error! Empty request!";
        return next();
    }
    const id = req.params.id;
    const newData = userService.changeUserData(id, data);
    if (typeof newData === "string") {
        req.err = newData;
    } else {
        req.new = newData;
    }
    next();
}

const deleteUser = (req, res, next) => {
    const id = req.params.id;
    const del = userService.deleteUser(id);
    if (typeof del === "string") {
        req.err = del;
    }else {
        req.new = del;
    }

    next();
}

const getUser = (req, res, next) => {
    const id = req.params.id;
    const usr = userService.getUser(id);
    if (typeof usr === "string") {
        req.err = usr;
    }else {
        req.new = usr;
    }

    next();
}

const getAllUsers = (req, res, next) => {
    req.new = userService.getAllUsers();
    next();
}


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.deleteUser = deleteUser;
exports.getUser = getUser;
exports.getAllUsers = getAllUsers;