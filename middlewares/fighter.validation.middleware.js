const { fighter } = require('../models/fighter');
const { FighterRepository } = require("../repositories/fighterRepository");
const FighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let fighterObj = validFighter(req.body);
    if (typeof fighterObj === "string") {
        req.err = fighterObj;
    } else {
        let newFighter;
        try {
            newFighter = FighterService.addFighter(fighterObj);
            req.new = newFighter
        }catch (err) {
            req.new= undefined;
            req.err = err;
        }
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const id = req.params.id;
    const data = req.body;
    const newData = updateFighterData(id, data);

    if (typeof newData === "string") {
        req.err = newData;
    } else {
        try {
            let updatedFighter = FighterService.updateFighter(id, newData);
            req.new = updatedFighter;
        }catch (err) {
            req.err = err;
            req.new = undefined;
        }
    }

    next();
}

function validFighter (fighterObj) {
    if (!fighterObj) {
        return "Error! Fighter is empty!";
    }
    const newFighterKeys = Object.keys(fighterObj);
    let newFighter = {};
    let check = 0;

    for (let i of newFighterKeys) {
        if (!(i in fighter)) {
            return `Error! Invalid parametr '${i}'!`;
        }
        switch (i) {
            case "id":
                return "Error! Parametr 'id' is not allowed!";
            case "health":
                if (!validHealth(fighterObj[i])) {
                    return `Error! Invalid property 'health'. Expect 80 < health < 120, get ${fighterObj[i]}`;
                }
                break;
            case "power":
                if (!(validPower(fighterObj[i]))) {
                    return `Error! Invalid property 'power'. Expect 1 < power < 10 get ${fighterObj[i]}`;
                }
                break;
            case "defense":
                if (!(validDefense(fighterObj[i]))){
                    return `Error! Invalid property 'defense'. Expect 1 < defense < 10 get ${fighterObj[i]}`;
                }
                break;
            case "name":
                if (!(validName(fighterObj[i]))) {
                    return `Error! Invalid property 'name'. It is already taken!`;
                }
                break;
        }
        newFighter[i] = fighterObj[i]
        ++check;
    }

    newFighter.health = newFighter.health || 100;

    if (!(check === 3 || check === 4)) {
        let error = "";
            for (let i of Object.keys(fighter)){
                if (!(i in fighterObj)) {
                    error += i + " ";
                }
            }
            return `Error. Not enough info. Missed ${error}`;
    }

    return newFighter;
}

function updateFighterData (id, data) {
    if (!(data && id)) {
        return "Error. Empty id or data!"
    }
    if (!(FighterService.search({"id": id}))) {
        return "Error! Fighter not found!";
    }

    let newData = {};
    let check = 0;
    let keys = Object.keys(data);
    for (let i of keys) {
        if (!(i in fighter)) {
            return `Error! Invalid parametr '${i}'`;
        }

        switch (i) {
            case "id":
                return "Error! Parametr 'id' is not allowed!";
            case "health":
                if (!validHealth(data[i])) {
                    return `Error! Invalid property 'health'. Expect 80 < health < 120, get ${data[i]}`;
                }
                break;
            case "power":
                if (!(validPower(data[i]))) {
                    return `Error! Invalid property 'power'. Expect 1 < power < 10 get ${data[i]}`;
                }
                break;
            case "defense":
                if (!(validDefense(data[i]))){
                    return `Error! Invalid property 'defense'. Expect 1 < defense < 10 get ${data[i]}`;
                }
                break;
            case "name":
                if (!(validName(data[i]))) {
                    if (!(FighterService.search({"id":id}).name === data.name)){
                        return `Error! Invalid property 'name'. It is already taken!`;
                    }
                }
                break;
        }
        ++check;
        newData[i] = data[i];
    }

    if (check === 0) {
        return "Error! No data to update!"
    }

    return newData;
}

const deleteFighter = (req, res, next) => {
    const id = req.params.id;
    const del = FighterService.deleteFighter(id);
    if (typeof del === "string") {
        req.err = del;
    }else {
        req.new = del;
    }

    next();
}

const getFighter = (req, res, next) => {
    const id = req.params.id;
    const fighterObj = FighterService.getFighter(id);
    if (typeof fighterObj === "string") {
        req.err = fighterObj;
    }else {
        req.new = fighterObj;
    }

    next();
}

const getAllFighters = (req, res, next) => {
    req.new = FighterService.getAllFighters();
    next();
}

function validPower (power) {
    if (!(power >= 1)) {
        return false;
    }
    if (!(power <= 100)) {
        return false;
    }
    return true;
}

function validHealth (health) {
    if (!(health >= 80)) {
        return false;
    }
    if (!(health <= 120)) {
        return false;
    }
    return true;
}

function validDefense (defense) {
    if (!(defense >= 1)) {
        return false;
    }
    if (!(defense <= 10)) {
        return false;
    }
    return true;
}

function validName (name) {
    for (let i of FighterRepository.getAll()) {
        if (i.name.toLocaleLowerCase() === name.toLocaleLowerCase()){
            return false;
        }
    }
    return true;
}

// exports.validators = {validPower,validHealth,validDefense};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.deleteFighter = deleteFighter;
exports.getFighter = getFighter;
exports.getAllFighters = getAllFighters;