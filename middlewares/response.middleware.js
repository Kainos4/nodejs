const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (req.err) {
        let code = 0;
        if ((/not found!$/).test(req.err)) {
            code = 404;
        } else {
            code = 400;
        }
        res.status(parseInt(code)).send(`{
            "error":true,
            "message":${req.err}
        }`);
    }else if (req.new) {
        res.status(200).send(req.new);
    }else {
        res.status(500).send("{'error':true, 'message':'Internal error!'}");
    }
    next();
}

exports.responseMiddleware = responseMiddleware;