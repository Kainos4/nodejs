const { FighterRepository } = require('../repositories/fighterRepository');
// const {fighter} = require("../models/fighter");

class FighterService {
    // TODO: Implement methods to work with fighters
    addFighter (fighterObj) {
        if (!fighterObj) {
            throw "Error! Fighter object is empty!!";
        }
        return FighterRepository.create(fighterObj);
    }

    updateFighter (id, data) {
        if (!data) {
            throw "Error! Data object is empty!!";
        }
        if (!(this.search({"id": id}))) {
            throw "Error! Fighter is not found!";
        }

        return FighterRepository.update(id, data);
    }

    deleteFighter (id) {
        if (!id) {
            return "Error! Empty id!";
        }
        if (!(this.search({"id" : id}))) {
            return "Error! Fighter not found!";
        }
        return FighterRepository.delete(id);
    }

    getFighter (id) {
        if (!id) {
            return "Error! Empty id!";
        }
        if (!(this.search({"id" : id}))) {
            return "Error! Fighter not found";
        }
        return this.search({"id" : id});
    }

    getAllFighters () {
        return FighterRepository.getAll();
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();