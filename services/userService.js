const { UserRepository } = require('../repositories/userRepository');
const {user} = require("../models/user");

class UserService {
    // TODO: Implement methods to work with user
    validUserAdd(_user) {
        const newUserKeys = Object.keys(_user);
        let newUser = {};
        let check = 0;
        for (let i of newUserKeys) {
            if (!(i in user)) {
                return `Error! Invalid parametr '${i}'`;
            }
            switch(i) {
                case "id":
                    return "Error! Parametr 'id' is not allowed!";
                case "firstName":
                    newUser.firstName = _user[i];
                    ++check;
                    break;
                case "lastName":
                    newUser.lastName = _user[i];
                    ++check;
                    break;
                case "email":
                    if (!this.validEmail(_user[i])){
                        return `Error. Invalid email ${_user[i]}`;
                    }
                    newUser.email = _user[i];
                    ++check;
                    break;
                case "phoneNumber":
                    if (!this.validPhone(_user[i])) {
                        return `Error. Invalid phone number ${_user[i]}`;
                    }
                    newUser.phoneNumber = _user[i];
                    ++check;
                    break;
                case "password":
                    if (!this.validPassword(_user[i])) {
                        return "Error. Password too short";
                    }
                    newUser.password = _user[i];
                    ++check;
                    break;
            }
        }
        if (check !== 5) {
            let error = "";
            for (let i of Object.keys(user)){
                if (!(i in _user)) {
                    error += i + " ";
                }
            }
            return `Error. Not enough info. Missed ${error}`;
        }

        if (this.search(newUser)) {
            return "Error user already exist";
        }

        if(this.emailRepets(newUser.email)){
            return "Error. Email already taken";
        }

        if (this.phoneRepets(newUser.phoneNumber)) {
            return "Error. Phone number already taken";
        }

        return UserRepository.create(newUser);
    }

    changeUserData(id, data) {
        if (!(data && id)) {
            return "Error. Empty id or data!"
        }
        if (!(this.search({"id": id}))) {
            return "Error! User not found!";
        }
        let newData = {};
        let check = 0;
        let keys = Object.keys(data);
        for (let i of keys) {
            if (!(i in user)) {
                return `Error! Invalid parametr '${i}'`;
            }

            switch (i) {
                case "id":
                    return "Error! Parametr 'id' is not allowed!";
                case "firsName":
                case "lastName":
                    break;
                case "email":
                    if (!(this.validEmail(data[i]))) {
                        return `Error! Invalid email ${data[i]}!`;
                    }
                    if (!(this.currentUserEmail(id, data[i]))) {
                        if (this.emailRepets(data[i])) {
                            return "Error! Email already taken!"
                        }
                    }
                    break;
                case "phoneNumber":
                    if (!(this.validPhone(data[i]))) {
                        return `Error! Invalid phone number ${data[i]}`;
                    }

                    if (!(this.currentUserPhone(id, data[i]))) {
                        if (this.phoneRepets(data[i])) {
                            return "Error! Phone number already taken!"
                        }
                    }
                    break;
                case "password":
                    if (!(this.validPassword(data[i]))){
                        return "Error! Incorect password!";
                    }
            }
            ++check;
            newData[i] = data[i];
        }

        if (check === 0) {
            return "Error! No data to update!"
        }
        
        return UserRepository.update(id, newData);
    }

    deleteUser (id) {
        if (!id) {
            return "Error! Empty id!";
        }
        if (!(this.search({"id" : id}))) {
            return "Error! User not found!";
        }
        return UserRepository.delete(id);
    }

    getUser (id) {
        if (!id) {
            return "Error! Empty id!";
        }
        if (!(this.search({"id" : id}))) {
            return "Error! User not found!";
        }
        return this.search({"id" : id});
    }

    getAllUsers () {
        return UserRepository.getAll();
    }

    currentUserEmail (id, mail) {
        if (this.search({"id":id}).email === mail) {
            return true;
        }
        return false;
    }

    currentUserPhone (id, phone) {
        if (this.search({"id":id}).phoneNumber === phone) {
            return true;
        }
        return false;
    }

    emailRepets (mail) {
        for (let i of UserRepository.getAll()) {
            if (i.email === mail) {
                return true;
            }
        }
        return false;
    }

    phoneRepets (phone) {
        for (let i of UserRepository.getAll()) {
            if (i.phoneNumber === phone) {
                return true;
            }
        }
        return false;
    }

    validEmail(mail){
        if (!mail){
            return false;
        }
        mail = mail.trim();
        const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+$/;
        let n = mail.indexOf("@");
        if (n === -1) {
            return false;
        }
        const name = mail.slice(0, n);
        if (!regex.test(name)) {
            return false;
        }
        if (!mail.endsWith("gmail.com")){
            return false;
        }
        return true;
    }

    validPhone(phone) {
        phone = String(phone);
        phone = phone.trim();
        if (phone.length !== 13) {
            return false;
        }
        const regex = /^\+380\d+$/;
        if (!regex.test(phone)) {
            return false;
        }
        return true;
    }

    validPassword(password) {
        if (!password || password.length < 3){
            return false;
        }
        return true;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();