const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid, deleteUser, getUser, getAllUsers } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

/*
exports.deleteUser = deleteUser;
exports.getUser = getUser;
exports.getAllUsers = getAllUsers;
 */

const router = Router();
    

    router.post("/", createUserValid, responseMiddleware);
    router.put("/:id", updateUserValid, responseMiddleware);
    router.delete("/:id", deleteUser, responseMiddleware);
    router.get("/", getAllUsers, responseMiddleware);
    router.get("/:id", getUser, responseMiddleware);
// TODO: Implement route controllers for user

module.exports = router;