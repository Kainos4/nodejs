const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid, deleteFighter, getFighter, getAllFighters } = require('../middlewares/fighter.validation.middleware');

const router = Router();

    router.post("/", createFighterValid, responseMiddleware);
    router.put("/:id", updateFighterValid, responseMiddleware);
    router.delete("/:id", deleteFighter, responseMiddleware);
    router.get("/", getAllFighters, responseMiddleware);
    router.get("/:id", getFighter, responseMiddleware);
// TODO: Implement route controllers for fighter

module.exports = router;